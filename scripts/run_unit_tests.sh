#!/bin/bash

# MIT License
# 
# Copyright (c) [year] [fullname]
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

pwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

dir=$1

if [ -z "$dir" ]; then
	echo "No unit tests dir provided"
	exit 1
fi

# dir=$pwd/../tests/UnitTests/bin/
resultsDir=$dir/results/

cd $dir

mkdir -p $resultsDir

unitTestsFailed=0

#Run tests
files=*
for unitTest in $files
do
	#if results or other dir then skip
	if [ -d "$unitTest" ]; then
		continue
	fi
	echo ""
	echo "Running $unitTest"
	./$unitTest -o $resultsDir/$unitTest.xml,xunitxml -o $resultsDir/$unitTest.txt,txt
	result=$?

	if [ "$result" -eq "0" ]; then
		echo "$unitTest: all tests passed"
	else
		echo "$unitTest: $result tests failed"
	fi

	unitTestsFailed=$(($unitTestsFailed + $result))
done

#Print summary
echo ""
if [ "$unitTestsFailed" -eq "0" ]; then
	echo "All unit tests passed"
else
	echo "$unitTestsFailed unit tests failed"
fi

#fix reports to match limitations from https://docs.gitlab.com/ee/ci/junit_test_reports.html#limitations
for file in $resultsDir/*.xml; 
do 
	className=${file#"$resultsDir/"}
	className=${className%".xml"}
	echo "$file      $className"

	oldString="<testcase"
	newString="<testcase classname=\"$className\""

	sed -i "s/$oldString/$newString/g" $file
done

cd -

exit $unitTestsFailed