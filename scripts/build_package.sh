#!/bin/bash

# MIT License
# 
# Copyright (c) [year] [fullname]
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set -e

pwd="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
projectDir=$pwd/../

binDir=$1
version=$2

if [ -z "$binDir" ]; then
	echo "No bin dir provided"
	exit 1
fi

if [ -z "$version" ]; then
	echo "No version provided"
	exit 1
fi

packageName=gitlabciqttemplate
packageDir=$projectDir/package/$packageName

packageFileName=$packageName-$version.deb
contentsPath=$packageDir/opt/GitLabCIQtTemplate/

mkdir -p $contentsPath

#copy binaries to package work dir
cp $binDir/* $contentsPath/

#update package version
sed -i "s/Version.*/Version: $version/g" $packageDir/DEBIAN/control

#make sure permissions to DEBIAN are correct
chmod -R 0755 $packageDir/DEBIAN

#cd to package dir
cd $packageDir/../

#build package
dpkg-deb --build $packageName

#rename package to have version in name
mv $packageName.deb $packageFileName

#move package file to artifacts dir
mv $packageFileName $binDir