/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef DURATIONSTRINGCONVERTER_H
#define DURATIONSTRINGCONVERTER_H

#include <QString>
#include <QDebug>

class DurationStringConverter
{
public:

    /*!
     * \brief secondsToString - converts amount of seconds to text formatted as '00:00:00'
     * \param seconds - amount of seconds to convert
     * \return formatted string if ok, else returns invalid string
     */
    static QString secondsToString(qint32 seconds)
    {
        if (seconds < 0) {
            qWarning() << "Negative duration not allowed";
            return QString();
        }
        qint32 hours = seconds / 3600;
        seconds = seconds % 3600;
        qint32 minutes = seconds / 60;
        seconds = seconds % 60;
        return QString("%1:%2:%3")
                .arg(hours, 2, 10, QChar('0'))
                .arg(minutes, 2, 10, QChar('0'))
                .arg(seconds, 2, 10, QChar('0'));
    }

    /*!
     * \brief stringToSeconds - converts text formatted as '00:00:00' to seconds
     * \param string - input string with format '00:00:00'
     * \return amount of seconds if string is valid or -1 if string is invalid
     */
    static qint32 stringToSeconds(const QString &string)
    {
        if (string.isEmpty() || !string.contains(QRegExp("\\d\\d\\:\\d\\d\\:\\d\\d"))) {
            qWarning() << "Wrong duration format" << string;
            return -1;
        }

        const QStringList &parts = string.split(':');
        if (parts.count() != 3) {
            qWarning() << "Wrong duration format" << string;
            return -1;
        }

        bool ok1 = false;
        bool ok2 = false;
        bool ok3 = false;
        qint32 hours = parts.at(0).toInt(&ok1);
        qint32 minutes = parts.at(1).toInt(&ok2);
        qint32 seconds = parts.at(2).toInt(&ok3);

        if (!(ok1 && ok2 && ok3)) {
            qWarning() << "Failed to parse duration string" << string;
            return -1;
        }

        return seconds + minutes * 60 + hours * 3600;
    }
};

#endif // DURATIONSTRINGCONVERTER_H
