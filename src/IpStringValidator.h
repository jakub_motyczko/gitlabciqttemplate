/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef IPSTRINGVALIDATOR_H
#define IPSTRINGVALIDATOR_H

#include <QString>

#include <arpa/inet.h>

class IpStringValidator
{
public:
    /*!
     * \brief validateIpv4 - checks if ipAddress string is a valid IPv4 address
     * \param ipAddress - string with ip address
     * \return true if valid, else false
     */
    static bool validateIpv4(const QString &ipAddress) {
        const QByteArray bytes = ipAddress.toUtf8();
        in_addr tmp;
        return inet_pton(AF_INET, bytes.constData(), &tmp) == 1;
    }

    /*!
     * \brief validateIpv6 - checks if ipAddress string is a valid IPv6 address
     * \param ipAddress - string with ip address
     * \return  true if valid, else false
     */
    static bool validateIpv6(const QString &ipAddress) {
        const QByteArray bytes = ipAddress.toUtf8();
        in6_addr tmp;
        return inet_pton(AF_INET6, bytes.constData(), &tmp) == 1;
    }
};

#endif // IPSTRINGVALIDATOR_H
