/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <QtTest>

#include "IpStringValidator.h"

class IpStringValidatorTest : public QObject
{
    Q_OBJECT

public:
    IpStringValidatorTest() {}
    ~IpStringValidatorTest() {}

private slots:
    void test_validateIpv4_data();
    void test_validateIpv4();
    void test_validateIpv6_data();
    void test_validateIpv6();

};

void IpStringValidatorTest::test_validateIpv4_data()
{
    QTest::addColumn<QString>("text");
    QTest::addColumn<bool>("expectedResult");

    //good
    QTest::addRow("loopback") << "127.0.0.1" << true;
    QTest::addRow("lan") << "192.168.0.1" << true;
    QTest::addRow("max values") << "255.255.255.255" << true;
    QTest::addRow("min values") << "0.0.0.0" << true;

    //bad
    QTest::addRow("lan - missing dot1") << "192.168.01" << false;
    QTest::addRow("lan - missing dot2") << "192168.0.1" << false;
    QTest::addRow("lan - missing part1") << "192.168.1" << false;
    QTest::addRow("lan - missing part2") << "192.168.0." << false;
    QTest::addRow("lan - spaces") << "192 168 0 1" << false;
    QTest::addRow("lan - commas") << "192,168,0,1" << false;
    QTest::addRow("lan - colons") << "192:168:0:1" << false;
    QTest::addRow("letters - good format") << "ab.ab.ab.ab" << false;
    QTest::addRow("letters - bad format") << "abababab" << false;
    QTest::addRow("empty string") << "" << false;
    QTest::addRow("too many segments") << "0.0.0.0.0" << false;
    QTest::addRow("negative numbers") << "-1.-1.-1.-1" << false;
    QTest::addRow("256-1") << "256.255.255.255" << false;
    QTest::addRow("256-2") << "255.256.255.255" << false;
    QTest::addRow("256-3") << "255.255.256.255" << false;
    QTest::addRow("256-4") << "255.255.255.256" << false;
    QTest::addRow("999-1") << "999.255.255.255" << false;
    QTest::addRow("999-2") << "255.999.255.255" << false;
    QTest::addRow("999-3") << "255.255.999.255" << false;
    QTest::addRow("999-4") << "255.255.255.999" << false;
}

void IpStringValidatorTest::test_validateIpv4()
{
    QFETCH(QString, text);
    QFETCH(bool, expectedResult);

    QCOMPARE(IpStringValidator::validateIpv4(text), expectedResult);
}

void IpStringValidatorTest::test_validateIpv6_data()
{
    QTest::addColumn<QString>("text");
    QTest::addColumn<bool>("expectedResult");

    //good
    QTest::addRow("good 1") << "2001:abcd::1111" << true;
    QTest::addRow("good 2") << "fe80::1111:2222:3333:4444" << true;
    QTest::addRow("good 3") << "2001:abcd::2222" << true;
    QTest::addRow("good 4") << "2001:abcd::3333" << true;
}

void IpStringValidatorTest::test_validateIpv6()
{
    QFETCH(QString, text);
    QFETCH(bool, expectedResult);

    QCOMPARE(IpStringValidator::validateIpv6(text), expectedResult);
}

QTEST_APPLESS_MAIN(IpStringValidatorTest)
#include "IpStringValidatorTest.moc"
