/*
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <QtTest>

#include "DurationStringConverter.h"

class DurationStringConverterTest : public QObject
{
    Q_OBJECT

public:
    DurationStringConverterTest() {}
    ~DurationStringConverterTest() {}

private slots:
    void test_secondsToString_data();
    void test_secondsToString();
    void test_stringToSeconds_data();
    void test_stringToSeconds();

};

void DurationStringConverterTest::test_secondsToString_data()
{
    QTest::addColumn<qint32>("duration");
    QTest::addColumn<QString>("expectedValue");

    QTest::newRow("duration 00:00:00") << 0 << "00:00:00";
    QTest::newRow("00:00:10") << 10 << "00:00:10";
    QTest::newRow("10:10:10") << 10 * 3600 + 10 * 60 + 10 << "10:10:10";
    QTest::newRow("23:59:59") << 23 * 3600 + 59 * 60 + 59 << "23:59:59";
    QTest::newRow("72:59:59") << 72 * 3600 + 59 * 60 + 59 << "72:59:59";
    QTest::newRow("99:59:59") << 99 * 3600 + 59 * 60 + 59 << "99:59:59";
    QTest::newRow("100:40:39") << 99 * 3600 + 99 * 60 + 99 << "100:40:39";
}

void DurationStringConverterTest::test_secondsToString()
{
    QFETCH(qint32, duration);
    QFETCH(QString, expectedValue);

    qDebug() << duration << expectedValue;

    QCOMPARE(DurationStringConverter::secondsToString(duration), expectedValue);
}

void DurationStringConverterTest::test_stringToSeconds_data()
{
    QTest::addColumn<QString>("duration");
    QTest::addColumn<qint32>("expectedValue");

    QTest::newRow("duration 00:00:00") << "00:00:00" << 0;
    QTest::newRow("00:00:10") << "00:00:10" << 10;
    QTest::newRow("10:10:10") << "10:10:10" << 10 * 3600 + 10 * 60 + 10;
    QTest::newRow("23:59:59") << "23:59:59" << 23 * 3600 + 59 * 60 + 59;
    QTest::newRow("72:59:59") << "72:59:59" << 72 * 3600 + 59 * 60 + 59;
    QTest::newRow("99:59:59") << "99:59:59" << 99 * 3600 + 59 * 60 + 59;
    QTest::newRow("100:40:39") << "100:40:39" << 99 * 3600 + 99 * 60 + 99;
}

void DurationStringConverterTest::test_stringToSeconds()
{
    QFETCH(QString, duration);
    QFETCH(qint32, expectedValue);

    qDebug() << duration << expectedValue;

    QCOMPARE(DurationStringConverter::stringToSeconds(duration), expectedValue);
}

QTEST_APPLESS_MAIN(DurationStringConverterTest)
#include "DurationStringConverterTest.moc"
