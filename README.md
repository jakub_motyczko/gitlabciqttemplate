# GitLab CI Template Qt project.

The purpose of this project is to provide a template GIT project for starting with new C++/Qt project that has support for GitLab CI with automatic compilation, running unit tests and building Debian packages.

This tutorial assumes that all commands are known to a user. If some command is unclear it's advised to search for it or use 'man' command in Bash.

Also user should get familiar with Docker and GitLab first.

What is Docker? What are images, containers etc? Please read more here: https://docs.docker.com/get-started/overview/

Most of the Bash scripts used in this template have a comments descripting what script does on particular line. Please check contents of scripts for better understanding.

Please check `.gitlab-ci.yml` to find out more about what's under the hood in GitLab CI scripts.

Please check `docker/Dockerfile` for more details about Docker image.

# 1. Copy repository

First we need to take code from this repository and make our target project.

Let's choose "NewCiProject" as a name of the new project.

## 1.1 Copy GIT project

1. Navigate to https://gitlab.com/projects/new
1. `Import project`
1. `Repo by URL`
1. Paste `https://gitlab.com/jakub_motyczko/gitlabciqttemplate.git` into `Git repository URL`
1. Paste `NewCiProject` into `Project name`
1. Pick `Project URL` that suits the needs best (group, change a slug if needed)
1. `Create project`

## 1.2 Update scripts

We need to replace all occurances of `GitLabCIQtTemplate` to `NewCiProject` - remember that casting has to be the same, so replace `gitlabciqttemplate` with `newciproject` and `GitLabCIQtTemplate` with `NewCiProject`. GIT clone NewCiProject now and modify following files:

1. scripts/build_package.sh
1. `mv package/gitlabciqttemplate package/newciproject`
1. package/newciproject/DEBIAN/control (change also Maintainer and other relevant stuff)


# 2. Install Qt libraries

This template project is based on Qt 5.11.1 installed in /opt/. If there'll be a need to use another Qt version or installation dir then it's required to modify `compile_app.sh` and `compile_unit_tests.sh` scripts according to the differences.

Download Qt installator from http://download.qt.io/new_archive/qt/5.11/5.11.1/

Other versions can be downloaded from:

* latest + LTS: https://download.qt.io/archive/qt/

* older: http://download.qt.io/new_archive/qt/

# 3. Docker part

We need to create Docker image and deploy it to GitLab registry.

## 3.1. Install & Start Docker

Visit https://docs.docker.com/engine/install/ and follow instructions.

## 3.2. Build Docker image

```
./scripts/create_docker_image_x86.sh
```

## 3.3. Push Docker image to GitLab

There are 2 ways to deal with Docker and GitLab marriage. Using existing shared GitLab runner (slow but easy to use and free to use with some limitations) described in 3.3.1 and using custom machine, or even own development PC, described in 3.3.2.

3.3.2 vs 3.3.1 (on i7-4700MQ) for this template project took 1:00 vs 3:14 so it was 3 times faster on `Specific Runner` than on `Shared Runner`.

### 3.3.1. Get docker image

GitLab will generate image name based on a project name picked in 1.1.

Navigate to `GitLab project` -> `Packages` -> `Container Registry` and grab image name. 

For the `NewCiProject` project of mine the image name is `registry.gitlab.com/jakub_motyczko/newciproject`. Yours will be `registry.gitlab.com/YOUR_USER_NAME_OR_GROUP_NAME_HERE/newciproject`.

```
sudo docker login registry.gitlab.com - use login and pass
sudo docker tag gitlabci-qt-builder-x86 registry.gitlab.com/jakub_motyczko/newciproject -- use your image name
sudo docker push registry.gitlab.com/jakub_motyczko/newciproject -- use your image name
```
Now refresh `GitLab project` -> `Packages` -> `Container Registry` website and check if your image appeared on GitLab.

Update `DOCKER_IMAGE_NAME` in `.gitlab-ci.yml` to image name obtained in 1.3.1. In my case: `registry.gitlab.com/jakub_motyczko/newciproject`

## 3.3.2. Use Docker image with your own GitLab Runner

As a host for GitLab Runner it's possible to use PC, VirtualMachine or even Docker image.

Instructions based on https://docs.gitlab.com/runner/install/docker.html and https://docs.gitlab.com/runner/register/

1. Start docker container 
	```
	sudo docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
	```
2. Navigate to `GitLab project` -> `CI/CD` -> `Runners`

3. `Shared Runners` - `Disable shared Runners`

4. `Group Runners` - `Disable group Runners`

5. `Specific Runners` - copy Runner token from `Set up a specific Runner manually`

6. Open terminal

7. Step into container bash with `sudo docker exec -it gitlab-runner bash`

8. Run `gitlab-runner register`

8.1. GitLab coordinator - https://gitlab.com/

8.2. gitlab-ci token -  token copied in step 6

8.3. gitlab-ci description - leave empty

8.4. gitlab-ci tags - leave empty

8.5. executor - docker

8.6. default Docker image - ruby:2.6

9. Navigate with browser to `GitLab project` -> `CI/CD` -> `Runners` and verify that runner has been registered in `Specific Runners`

# 4. Test Building and packaging

1. Navigate to `GitLab project` -> `Repository` -> `Tags`
1. `New tag`
1. set `Tag name` to 0.1.0 (or some other version string you want, but it has start with numeric value)
1. `Create tag`
1. Navigate to `GitLab project` -> `CI/CD` -> `Pipelines` and see if Pipeline passed

Push some changes to GitLab now and navigate to pipelines website to see if the Pipeline started and/or passed.


